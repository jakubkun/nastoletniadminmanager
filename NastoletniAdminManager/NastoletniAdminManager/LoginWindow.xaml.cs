﻿using NastoletniAdminManagerLib;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NastoletniAdminManager
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : ManagerWindow
    {
        public LoginWindow()
        {
            InitializeComponent();
            DataContext = this;
            this.ResizeMode = System.Windows.ResizeMode.NoResize;

            LoginButton.SetText("LOGIN");
        }

        private void LoginButton_Loaded(object sender, RoutedEventArgs e)
        {
            UserData UD = new UserData();
            bool exception = false;
            try
            {
                UD.Name = "Jan";
                UD.Reason = "Brak";
                UD.Type = 0;

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://api.nastoletni.pl/v1/bans");
                request.ContentType = "application/json";
                request.Method = "POST";

                String username = Login.Text.ToString();
                String password = Password.Password.ToString();
                String encoded = System.Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));
                
                request.Headers.Add("Authorization", "Basic " + encoded);

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(UD.Date)).TotalSeconds;
                    string json = "{\"name\":\"" + UD.Name + "\",\"reason\":\"" + UD.Reason + "\",\"by\":\"1\",\"when\":" + unixTimestamp + ",\"type\":" + UD.Type + "}";

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                try
                {
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                }
                catch (WebException we)
                {
                    HttpWebResponse response = (HttpWebResponse)we.Response;
                }
                finally
                {
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Bad Password");
                exception = true;
            }
            finally
            {
                if (!exception)
                {
                    MessageBox.Show("Good Password");

                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://api.nastoletni.pl/v1/bans");
                    request.ContentType = "application/json";
                    request.Method = "GET";

                    String username = Login.Text.ToString();
                    String password = Password.Password.ToString();
                    String encoded = System.Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));

                    request.Headers.Add("Authorization", "Basic " + encoded);

                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                    StreamReader reader = new StreamReader(response.GetResponseStream());

                    string responseString = reader.ReadToEnd();

                    UserData[] deserializedData = JsonConvert.DeserializeObject<UserData[]>(responseString);
                    List<UserData> TableData;
                    TableData = deserializedData.ToList();

                    UserData DT = TableData[TableData.Count - 1];
                     //DELETE

                    HttpWebRequest request1 = (HttpWebRequest)WebRequest.Create("http://api.nastoletni.pl/v1/bans/" + DT.ID);
                    request1.ContentType = "application/json";
                    request1.Method = "DELETE";

                    String encoded1 = System.Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));

                    request1.Headers.Add("Authorization", "Basic " + encoded);

                    using (var streamWriter = new StreamWriter(request1.GetRequestStream()))
                    {
                        Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(UD.Date)).TotalSeconds;
                        string json = "{\"name\":\"" + DT.Name + "\",\"reason\":\"" + DT.Reason + "\",\"by\":\"1\",\"when\":" + unixTimestamp + ",\"type\":" + DT.Type + "}";

                        streamWriter.Write(json);
                        streamWriter.Flush();
                        streamWriter.Close();
                    }

                    try
                    {
                        HttpWebResponse response1 = (HttpWebResponse)request1.GetResponse();
                    }
                    catch (WebException we)
                    {
                        HttpWebResponse response1 = (HttpWebResponse)we.Response;
                    }
                    finally
                    {
                        HttpWebResponse response1 = (HttpWebResponse)request1.GetResponse();
                    }

                    MainWindow mw = new MainWindow(username,password);
                    mw.Show();

                    this.Close();
                }
            }
        }
    }
}
