﻿using NastoletniAdminManagerLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace NastoletniAdminManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : NastoletniAdminManagerLib.ManagerWindow
    {
        //public List<UserData> TableData;

        private string Login;
        private string Password;

        public void ShowSidebarEdition(UserData data)
        {
            SidebarContainer.Visibility = System.Windows.Visibility.Visible;
            FillSidebar(data);
            SEND.SetText("Zmień");
            
        }

        public void FillSidebar(UserData data)
        {
            SidebarData = data;
            NameTextBox.Text = data.Name;
            ReasonTextBox.Text = data.Reason;
        }

        public MainWindow(string log,string pas)
        {
            InitializeComponent();
            DataContext = this;

            POST.SetText("POST");
            BAN.SetText("BAN");
            PBAN.SetText("PERM");
            SEND.SetText("Wyślij");

            SidebarData = new UserData();

            MainTable.OnEdit += (UD) => 
            {
                ShowSidebarEdition(UD);
                    ActionTitle.Text = "Edytuj " + UD.Name; 
            };

            this.Login = log;
            this.Password = pas;

            MainTable.SetLoginData(Login, Password);

            MainTable.Refresh();
        }

        public UserData SidebarData;

        public DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            SEND.SetText("Wyślij");
            ActionTitle.Text = "Dodaj nowy...";
            if (SidebarContainer.IsVisible)
            {
                SidebarContainer.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                SidebarContainer.Visibility = System.Windows.Visibility.Visible;
            }
            SidebarData = new UserData();
            ReasonTextBox.Text = SidebarData.Reason;
            NameTextBox.Text = SidebarData.Name;
        }

        private void ManagerWindow_SizeChanged_1(object sender, SizeChangedEventArgs e)
        {
            if (WindowState == System.Windows.WindowState.Maximized)
            {
                Refresh.Margin = new Thickness(7, 0, 0, 0);
            }
            else
            {
                Refresh.Margin = new Thickness(0, 0, 0, 0);
            }
        }

        private void PBAN_Loaded(object sender, RoutedEventArgs e)
        {
            SidebarData.Type = 0;

            PBAN.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#8e44ad"));

            BAN.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#E84C3D"));
            POST.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#27AE61"));
        }

        private void BAN_Loaded(object sender, RoutedEventArgs e)
        {
            SidebarData.Type = 1;


            BAN.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#8e44ad"));

            PBAN.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#C23A2C"));
            POST.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#27AE61"));
        }

        private void POST_Loaded(object sender, RoutedEventArgs e)
        {
            SidebarData.Type = 2;

            POST.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#8e44ad"));

            PBAN.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#C23A2C"));
            BAN.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#E84C3D"));
        }

        private void SEND_Loaded(object sender, RoutedEventArgs e)
        {
            SidebarData.Name = NameTextBox.Text;
            SidebarData.Reason = ReasonTextBox.Text;
            SidebarData.Date = DateTime.Now;

            if (SEND.GetText() == "Wyślij")
            {
                MainTable.Send(SidebarData,Login,Password);
                MessageBox.Show("Wysłano");
            }
            else if (SEND.GetText() == "Zmień")
            {
                MessageBox.Show("Zmieniono");
                MainTable.Modify(SidebarData,Login,Password);
            }
        }

        private void Refresh_Click_1(object sender, RoutedEventArgs e)
        {
            MainTable.Refresh();
        }
    }
}