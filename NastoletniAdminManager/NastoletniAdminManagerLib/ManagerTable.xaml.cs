﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NastoletniAdminManagerLib
{
    /// <summary>
    /// Interaction logic for ManagerTable.xaml
    /// </summary>
    public partial class ManagerTable : UserControl
    {  
        public ManagerTable()
        {
            InitializeComponent();
        }

        public string user;
        public string pas;

        public void SetLoginData(string username, string pass)
        {
            this.user = username;
            this.pas = pass;
        }
        public static TableRecord selectedRecord;

        public event Action<UserData> OnEdit;

        public void EditDATA(UserData UD)
        {
            if (OnEdit != null)
            {
                OnEdit(UD);
            }
        }

        public void AddChild(TableRecord newChild)
        {
            Content.Children.Add(newChild);
            newChild.ParentTable = this;
        }


        /// <summary>
        /// Łączy się z serwerem i pobiera tablice banów, deserializuje ją i wkłada do tabeli
        /// </summary>
        public void Refresh()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://api.nastoletni.pl/v1/bans");
            request.ContentType = "application/json";
            request.Method = "GET";

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            StreamReader reader = new StreamReader(response.GetResponseStream());

            string responseString = reader.ReadToEnd();

            UserData[] deserializedData = JsonConvert.DeserializeObject<UserData[]>(responseString);
            List<UserData> TableData;
            TableData = deserializedData.ToList();

            this.RemoveAllChildren();

            foreach (var i in TableData)
            {
                this.AddChild(new TableRecord(i));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public void Send(UserData UD,string user,string pass)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://api.nastoletni.pl/v1/bans");
            request.ContentType = "application/json";
            request.Method = "POST";

            String username = user;
            String password = pass;
            String encoded = System.Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));

            request.Headers.Add("Authorization", "Basic " + encoded);

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(UD.Date)).TotalSeconds;
                string json = "{\"name\":\"" + UD.Name + "\",\"reason\":\"" + UD.Reason + "\",\"by\":\"1\",\"when\":" + unixTimestamp + ",\"type\":" + UD.Type +"}";

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException we)
            {
                HttpWebResponse response = (HttpWebResponse)we.Response;
            }
            finally
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            }

            Refresh();
        }

        public void Modify(UserData UD,string user,string pas)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://api.nastoletni.pl/v1/bans/"+ UD.ID);
            request.ContentType = "application/json";
            request.Method = "PATCH";

            String username = user;
            String password = pas;
            String encoded = System.Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));

            request.Headers.Add("Authorization", "Basic " + encoded);

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(UD.Date)).TotalSeconds;
                string json = "{\"name\":\"" + UD.Name + "\",\"reason\":\"" + UD.Reason + "\",\"by\":\"1\",\"when\":" + unixTimestamp + ",\"type\":" + UD.Type + "}";

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException we)
            {
                HttpWebResponse response = (HttpWebResponse)we.Response;
            }
            finally
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            }

            Refresh();
        }

        public void RemoveAllChildren()
        {
            Content.Children.Clear();
        }

        public void Edit(UserData UD)
        {
            Application.Current.MainWindow.Focus();
        }
      
        public void Delete(UserData UD,string user,string pas)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://api.nastoletni.pl/v1/bans/" + UD.ID);
            request.ContentType = "application/json";
            request.Method = "DELETE";

            String username = user;
            String password = pas;
            String encoded = System.Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));

            request.Headers.Add("Authorization", "Basic " + encoded);

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(UD.Date)).TotalSeconds;
                string json = "{\"name\":\"" + UD.Name + "\",\"reason\":\"" + UD.Reason + "\",\"by\":\"1\",\"when\":" + unixTimestamp + ",\"type\":" + UD.Type + "}";

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException we)
            {
                HttpWebResponse response = (HttpWebResponse)we.Response;
            }
            finally
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            }

            Refresh();
        }
    }
}
