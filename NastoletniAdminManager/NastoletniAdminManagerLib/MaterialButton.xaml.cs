﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NastoletniAdminManagerLib
{
    /// <summary>
    /// Interaction logic for MaterialButton.xaml
    /// </summary>
    public partial class MaterialButton : UserControl
    {
        private double _Opacity;

        public double TargetWidth = 500.0;
        public double TargetHeight = 250.0;

        public double DefaultWidth;
        public double DefaultHeight;
      
        public MaterialButton()
        {
            InitializeComponent();
            _Opacity = 0.3; 
            SetTextColor(Brushes.White);
            SetRippleColor(Brushes.White);
           
        }


        public void AnimateRipple(double from, double to, UIElement control, double duration)
        {
            var heightAnimation = new DoubleAnimation
            {
                From = from,
                To = to,
                Duration = TimeSpan.FromSeconds(duration)
            };
            Storyboard.SetTarget(heightAnimation, control);
            Storyboard.SetTargetProperty(heightAnimation, new PropertyPath(FrameworkElement.HeightProperty));

            //Szerokość

            var widthAnimation = new DoubleAnimation
            {
                From = from,
                To = to,
                Duration = TimeSpan.FromSeconds(duration)
            };
            Storyboard.SetTarget(widthAnimation, control);
            Storyboard.SetTargetProperty(widthAnimation, new PropertyPath(FrameworkElement.WidthProperty));


            var sb = new Storyboard();
            sb.Children.Add(widthAnimation);
            sb.Children.Add(heightAnimation);


            sb.Completed +=
                (o, e1) =>
                {
                    AnimateFade(control.Opacity, 0, control, 0.4);
                };
            sb.Begin();
        }
        public static void AnimateFade(double from, double to, UIElement control, double duration)
        {

            var opacityAnimation = new DoubleAnimation
            {
                From = from,
                To = to,
                Duration = TimeSpan.FromSeconds(duration)
            };
            Storyboard.SetTarget(opacityAnimation, control);
            Storyboard.SetTargetProperty(opacityAnimation, new PropertyPath(UIElement.OpacityProperty));

            var sb = new Storyboard();
            sb.Children.Add(opacityAnimation);
            sb.Begin();

        }
        private void UserControl_PreviewMouseDown(object sender, MouseButtonEventArgs e) //Kliknięcie myszką
        {
            var targetWidth = Math.Max(ActualHeight, ActualWidth * 2); 
            AnimateFade(0, _Opacity, Ripple, 0.1); 
            Ripple.Height = 0; 
            Ripple.Width = 0; 
            AnimateRipple(0, targetWidth, Ripple, 0.6);           
        }

        public void SetTextColor(Brush brush){ textBlock.Foreground = brush; } 
        public void SetText(string text){ textBlock.Content = text; }
        public string GetText() { return textBlock.Content.ToString(); }
        public void SetRippleColor(Brush brush){ Ripple.Fill = brush; } 
        public void SetRippleOpacity(double opacity){ Ripple.Opacity = opacity; _Opacity = opacity; }
    }
}

