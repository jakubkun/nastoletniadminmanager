﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;


namespace NastoletniAdminManagerLib
{
    public class UserData
    {
        public string Name { get; set; }
        public int Type { get; set; }
        public string Reason { get; set; }
        public DateTime Date { get; set; }
        public int AuthorID { get; set; }
        public int ID { get; set; }

        public UserData()
        {
            ColorDictionary = new Dictionary<string, Color>();

            ColorDictionary.Add("PERM", (Color)ColorConverter.ConvertFromString("#C23A2C")); //0
            ColorDictionary.Add("BAN", (Color)ColorConverter.ConvertFromString("#E84C3D")); //1
            ColorDictionary.Add("POST", (Color)ColorConverter.ConvertFromString("#27AE61")); //2
        }

        public UserData(int Type, string Name, string Reason)
        {
            this.Type = Type;
            this.Name = Name;
            this.Reason = Reason;

            ColorDictionary = new Dictionary<string, Color>();

            ColorDictionary.Add("PERM", (Color)ColorConverter.ConvertFromString("#C23A2C")); //0
            ColorDictionary.Add("BAN", (Color)ColorConverter.ConvertFromString("#E84C3D")); //1
            ColorDictionary.Add("POST", (Color)ColorConverter.ConvertFromString("#27AE61")); //2
        }

        public Dictionary<string, Color> ColorDictionary;
    }
}

/*
 * Type:
 * = 0 - PERM
 * = 1 - BAN
 * = 2 - Post usunięty
 */


