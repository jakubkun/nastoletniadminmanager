﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace NastoletniAdminManagerLib
{
    public class ManagerWindow : Window, INotifyPropertyChanged
    {
        protected override void OnStateChanged(EventArgs e)
        {
            base.OnStateChanged(e);
            OnPropertyChanged("CaptionButtonMargin");
        }

        public Thickness CaptionButtonMargin
        {
            get
            {
                if (WindowState == WindowState.Maximized)
                    return new Thickness(6, 8, 8, 0);
                else
                    return new Thickness(0, 0, 0, 0);
            }
        }

        private void OnPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
