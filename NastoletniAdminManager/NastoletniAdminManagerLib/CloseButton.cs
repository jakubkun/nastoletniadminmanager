﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace NastoletniAdminManagerLib
{
    public class CloseButton : CaptionButton
    {
        static CloseButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(CloseButton), new FrameworkPropertyMetadata(typeof(CloseButton)));
        }

        protected override void OnClick()
        {
            base.OnClick();
            SystemCommands.CloseWindow(Window.GetWindow(this));
        }
    }
}
