﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NastoletniAdminManagerLib
{
    /// <summary>
    /// Interaction logic for TableRecord.xaml
    /// </summary>
    public partial class TableRecord : UserControl
    {
        public ManagerTable ParentTable;

        public TableRecord()
        {
            InitializeComponent();
            DataContext = this;

            Edit.SetText("Edytuj");
            Delete.SetText("Usuń");
        }

       public TableRecord(UserData UD)
        {
            InitializeComponent();
            this.Data = UD;
            DataContext = this;

            Edit.SetText("Edytuj");
            Delete.SetText("Usuń");
        }
        
        public UserData _data;
        public Color TypeColor;

        public UserData Data
        {
            get { return _data; }
            set
            {
                _data = value;

                this.Type.Text = _data.Type.ToString();
                this.Name.Text = _data.Name.ToString();
                this.Reason.Text = _data.Reason.ToString();
                TypeFill.Fill = new SolidColorBrush((Color)_data.ColorDictionary.Values.ElementAt(int.Parse(Type.Text)));
                Type.Text = _data.ColorDictionary.Keys.ElementAt(int.Parse(Type.Text));
            }
        }

        private void Delete_PreviewMouseDown_1(object sender, MouseButtonEventArgs e)
        {
               ParentTable.Delete(Data, ParentTable.user, ParentTable.pas);      
        }

        private void Edit_PreviewMouseDown_1(object sender, MouseButtonEventArgs e)
        {
            ParentTable.EditDATA(_data);
        }
    }
}
