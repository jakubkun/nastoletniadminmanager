﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace NastoletniAdminManagerLib
{
    public class AddButton : CaptionButton
    {
        static AddButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(AddButton),new FrameworkPropertyMetadata(typeof(AddButton)));
        }

        protected override void OnClick()
        {
            base.OnClick();
        }
    }
}
